// USB Connection Stuff
var SerialPort = require("serialport");
var serialport = new SerialPort("COM3", {
	baudRate: 9600,
	parser: SerialPort.parsers.readline("\r\n"),
	parity: 'none'
	});

var tempStarting = true;

var socket = require('socket.io-client')('http://192.168.0.71');

socket.on('connect', function(){
	console.log("Socket Connected!");
	socket.emit('set nick', "Lane-01");
	socket.emit('set lane', 1);
	socket.emit('send message', "I just connected!");
});

serialport.on('open', function(){
  console.log('USB Connected!');
	// -- Connect to the socket.io server (and assign itself a lane)
});

serialport.on('data',function(data){
   	// for now, we're just forwarding anything coming OUT of Arduino as a message
   		console.log(data);
   		
		// -- the Arduino will send messages for START and finish
		// -- this HIT switch method is for debugging before programming arduino

   		if (data == "HIT") {
   			if (tempStarting) {
   				socket.emit('lane start');
   				tempStarting=false;
   			} else {
   				socket.emit('lane finish');
   				tempStarting=true;
   			}
   		}
		
/*
		I decided to do the date() calculations that calculate time from
		start to finish on the server because the server would be able
		to have more flow control over the race.  That is, the server
		can allow or disallow races to start, which is harder to implement
		if each client is reporting its times.
*/

   		if (data == "START") {
   			
   			 socket.emit('lane start'); // not coded yet in server
   		}

		// -- If FINISH, emit a 'new message' finishing the race for its lane
   		if (data == "FINISH") {
   			socket.emit('lane finish'); // not coded yet in server
   		}

   	
   		
});


