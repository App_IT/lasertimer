var url = require('url'),
	express = require('express'),
	app = express(),
	server = require('http').createServer(app),
	io = require('socket.io').listen(server),
	port = 80,
	users = {},
	queryObject=undefined,
	nick,
	races = [],
	race_start_time = [],
	allow_start = [],
	allow_finish = [],
	counter = 1;

	server.listen(port);
	console.log("Server Listening on port: " + port);

	for (i=0;i<10;i++) {
		allow_start[i]=false;
		allow_finish[i]=false;
	}

	app.get('/', function(req, res){
		res.sendFile(__dirname + '/index.html');
		queryObject = url.parse(req.url,true).query;
		if (!queryObject.name) {
			nick = undefined;
		} else {
			nick = queryObject.name;
		}
	});

	app.get('/results', function(req, res){
		//		res.sendFile(__dirname + '/results.html');
		var html = "<div id = 'results' style = 'margin-left: auto; margin-right: auto; width: 600px; margin-top: 20px'><h3>RESULTS:</h3><table>";

		for (i=1;i<=8;i++) {
			html += "<tr><td style = 'padding: 5px; font-size: 22; border: solid 1px'>Lane " + i + "</td><td style = 'padding: 5px; font-size: 22; border: solid 1px'>" + races[i] + " seconds</td>";
		}

		html += "</table>"
		res.send(html);
	});

// CREATE SOCKETS

	io.sockets.on('connection', function(socket){
	
		function updateNicknames() {
			//io.sockets.emit('usernames', Object.keys(users));
			io.sockets.emit('usernames', Object.keys(users));			
		}

		if (nick == undefined) {
			console.log("Name not found in URL, assigning random username");
			var rand = Math.floor(Math.random() * 8999) + 1000;
			socket.nickname="RM-" + rand;

			// if the name happens to be used (1 in 9999 chance!), keep generate another until a unique name is found
			while(socket.nickname in users) {
				var rand = Math.floor(Math.random() * 8999) + 1000;
				socket.nickname="RM-" + rand;
			}
			
			users[socket.nickname] = socket;
			updateNicknames();	
			console.log(socket.nickname + " connected.");

		} else {
			console.log("Nickname found (in URL or from reconnect)");
			socket.nickname = nick;
			//queryObject=undefined;
			
			// if the user isn't already connected, add them to the users array
			if (socket.nickname in users) {
				// here you could disconnect the previous instance or show an error that they're already connected
				console.log("This user has the same name as an existing user.  An indexer will be appended.");
				socket.nickname = socket.nickname + "-" + counter.toString();
				counter++;
			}

			users[socket.nickname] = socket;
			
			updateNicknames();	
			console.log(socket.nickname + " connected.");
			}

			io.sockets.emit('new message', {msg: socket.nickname + " connected!", nick: "[server]"});

// MESSAGING HANDLER
	
		socket.on('set robot', function(data){
			
			// Change this so you can set robots indexed by lanes
			socket.robot = data;
			io.sockets.emit('new message', {msg: socket.nickname + " now holds a robot: <b>" + socket.robot + "</b>", nick: "[server]"});
			updateNicknames();
		});

		socket.on('set nick', function(data){
			
			// Change this so you can set robots indexed by lanes
			var oldName = socket.nickname;
			delete users[socket.nickname];
			socket.nickname = data;

			users[socket.nickname] = socket;
			
			console.log(oldName + " is now called: " + socket.nickname);
			io.sockets.emit('new message', {msg: oldName + " is now set to " + socket.nickname + "</b>", nick: "[server]"});
			updateNicknames();
		});

		socket.on('send message', function(data){
			io.sockets.emit('new message', {msg: data, nick: socket.nickname});
			updateNicknames();
		});		

		socket.on('start race', function(){
			for (i=0;i<7;i++) {
				allow_start[i] = true;
				allow_finish[i] = false;
			}
			var message = "Races can now start!!";
			io.sockets.emit('new message', {msg: message, nick: "[server]"});

		});

		socket.on('set lane', function(data){
			socket.lane = data;
			console.log(socket.nickname + " is now set to a lane -- " + socket.lane);
		});

		socket.on('lane start', function(){
			/* Lanes and Nicknames should be generally the same, except 
			socket.lane is an integer, and socket.nickname is a string
			only socket.nickname appears in the control page */
			var lane = socket.lane;

			if (allow_start[lane]) {
				console.log("Start Line Reported! (Lane: " + lane + ")");

				race_start_time[lane] = new Date();

				var myStartTime = race_start_time[lane];
				var formatted_start_time = myStartTime.getHours() + ":" + myStartTime.getMinutes() + ":" + myStartTime.getMilliseconds();
				io.sockets.emit('new message', {msg: "Lane " + lane + " -- Start Line Crossed at: " + formatted_start_time, nick: "[server]"});
				allow_start[lane] = false;
				allow_finish[lane] = true;
			} else {
				console.log("Can't start yet. Start a race in WebUI (Lane: " + lane + ")");
			}

		});


		socket.on('lane finish', function(){
			
			var lane = socket.lane;

			if (allow_finish[lane]) {
				console.log("Finish Line Reported! (Lane: " + lane + ")");
			
				var myFinishTime = new Date();
				var elapsed_time = myFinishTime - race_start_time[lane];
				var formatted_finish_time = elapsed_time / 1000;
			
				io.sockets.emit('new message', {msg: "<b>Lane " + lane + "</b> -- finished in <b>" + formatted_finish_time + "</b>", nick: "[server]"});

				console.log("Lane: " + lane + " | " + "Time: " + formatted_finish_time);

				races[lane] = formatted_finish_time;
				allow_finish[lane] = false;
				allow_start[lane] = false;

			} else {
				console.log("Didn't start; can't finish (Lane: " + lane + ")");
			}

		});

// DISCONNECT HANDLER
	
		socket.on('disconnect', function(data){
			if(!socket.nickname) return;
			console.log(socket.nickname + " disconnected");
			io.sockets.emit('new message',{msg: "<b>" + socket.nickname + " disconnected!</b>", nick: "[server]"});			
			delete users[socket.nickname];
			updateNicknames();			
		});

	});