
int counter = 0;
int reading_A0 = 999;
int reading_A5 = 999;

void setup() {
  pinMode(A0, INPUT);
  pinMode(A5, INPUT);
  delay(100);
  Serial.begin(9600);
  Serial.print("Initial reading (A0): ");Serial.println(analogRead(0));
  Serial.print("Initial reading (A5): ");Serial.println(analogRead(5));
  delay(1000);
}

void loop() {
  reading_A0 = analogRead(0);
  reading_A5 = analogRead(5);
  //Serial.println("A0 ----- A5");
  //Serial.print(reading_A0);Serial.print(" ----- ");Serial.println(reading_A5);
  // If A0 light drops, the Start Line was crossed.  Send a message to the NodeJS client.
  if (reading_A0 < 400) {
    Serial.println("START");

    // debounce (wait until light comes back, then wait 1 sec more
    while (reading_A0 < 400) {
      reading_A0 = analogRead(0);
      //Serial.println(reading);
      delay(10);
      digitalWrite(13, HIGH);
    }
    delay(1000);
    digitalWrite(13, LOW);
  }


  // If A5 light drops, the Finish Line was crossed.  Send a message to the NodeJS client.
  if (reading_A5 < 400) {
    Serial.println("FINISH");

    // debounce (wait until light comes back, then wait 1 sec more
    while (reading_A5 < 400) {
      reading_A5 = analogRead(5);
      //Serial.println(reading);
      delay(10);
      digitalWrite(13, HIGH);
    }
    delay(1000);
    digitalWrite(13, LOW);
  }
  
  delay(1);
}
